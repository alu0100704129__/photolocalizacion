-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-12-2014 a las 13:12:07
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE IF NOT EXISTS `imagenes` (
  `nombre` varchar(20) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `longitud` double NOT NULL,
  `latitud` double NOT NULL,
  `altitud` double NOT NULL,
  `ancho` int(11) NOT NULL,
  `alto` int(11) NOT NULL,
  `peso` double NOT NULL,
  `modelo` varchar(200) NOT NULL,
  `camara` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`nombre`, `usuario`, `foto`, `tipo`, `longitud`, `latitud`, `altitud`, `ancho`, `alto`, `peso`, `modelo`, `camara`) VALUES
('2014-12-08 17.40.40.', 'adriana', '../imagenes/2014-12-08 17.40.40.jpg', 'image/jpeg', -16.55, 28.397222222222, 0.0, 2048, 1536, 0.72, 'GT-S5830', 'SAMSUNG'),
('mohammed.jpg', 'adriana', '../imagenes/mohammed.jpg', 'image/jpeg', -16.316666666667, 28.480833333333, 0.0, 1920, 2560, 1.16, 'GT-S5830', 'SAMSUNG'),
('oliver.jpg', 'adriana', '../imagenes/oliver.jpg', 'image/jpeg', -16.316666666667, 28.480833333333, 0.0, 1920, 2560, 1.22, 'GT-S5830', 'SAMSUNG'),
('rafa.jpg', 'adriana', '../imagenes/rafa.jpg', 'image/jpeg', -16.316666666667, 28.480833333333, 0.0, 1920, 2560, 1.18, 'GT-S5830', 'SAMSUNG');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
 ADD PRIMARY KEY (`nombre`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
